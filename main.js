let loadingPosts = false;
let names = {};

// todo Вынести html элементы в отдельный файл и функции в отдельный файл

const spinner =
	"<br />" +
	"<br />" +
	"<center>" +
	"   <div class=\"preloader-wrapper active\">\n" +
	"    <div class=\"spinner-layer spinner-teal-only\">\n" +
	"      <div class=\"circle-clipper left\">\n" +
	"        <div class=\"circle\"></div>\n" +
	"      </div><div class=\"gap-patch\">\n" +
	"        <div class=\"circle\"></div>\n" +
	"      </div><div class=\"circle-clipper right\">\n" +
	"        <div class=\"circle\"></div>\n" +
	"      </div>\n" +
	"    </div>\n" +
	"  </div>" +
	"</center>" +
	"<br />" +
	"<br />";

function loadNames(){
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open("GET", "/static/names.json");
        xhr.send(null);
        xhr.onreadystatechange = () => {
            if(xhr.readyState === XMLHttpRequest.DONE){
                names = JSON.parse(xhr.responseText);
                resolve();
            }
        }
    });
}

function loadRecent(){
	const xhr = new XMLHttpRequest();
	xhr.open("GET", "/api/?method=recent");
	xhr.send(null);
	const recent = e("#response");
	recent.innerHTML = spinner;
	xhr.onreadystatechange = () => {
		if(xhr.readyState === XMLHttpRequest.DONE){
			let readyHtml = "";
			const response = JSON.parse(xhr.responseText);
			if(response.status){
				for (const post of response.response.items) {
					readyHtml += "<a href=\"" + post.link + "\" class=\"collection-item waves-effect\">" + post.name + "</a>"
				}
				response.innerHTML = readyHtml;
			}else{
				Toast.error("Can't load recent due to "+response.error);
			}
		}
	}
}

function makePost(name, classes, date_start, date_end, location, lesson, education_centre, short_text, link){
    return "<div class=\"card col s12 hoverable\" onclick='window.location.href = \"" + link + "\"'>\n" +
        "     <div class=\"row no-bottom-margin\">\n" +
        "      <div class=\"col s12 m12 l5\">\n" +
        "       <ul class=\"collection collection-noborder no-bottom-margin\">\n" +
        "        <li class=\"collection-item avatar age\">\n" +
        "         <i class=\"material-icons circle  teal lighten-1\">child_care</i>\n" +
        "         <span>Возраст:</span>\n" +
        "         <p>" + classes + " Класс</p>\n" +
        "        </li>\n" +
        "        <li class=\"collection-item avatar\">\n" +
        "         <i class=\"material-icons circle  teal lighten-1\">date_range</i>\n" +
        "         <span>Дата: </span>\n" +
        "         <p>" + date_start + " -  " + date_end + " </p>\n" +
        "        </li>\n" +
        "        <li class=\"collection-item avatar\">\n" +
        "         <i class=\"material-icons circle  teal lighten-1\">location_city</i>\n" +
        "         <span>Место проведения:</span>\n" +
        "         <p>" + location + "</p>\n" +
        "        </li>\n" +
        "        <li class=\"collection-item avatar\">\n" +
        "         <i class=\"material-icons circle teal lighten-1\">library_books</i>\n" +
        "         <span>Дисциплина:</span>\n" +
        "         <p>" + lesson + "</p>\n" +
        "        </li>\n" +
        "        <li class=\"collection-item avatar\">\n" +
        "         <i class=\"material-icons circle teal lighten-1\">location_on</i>\n" +
        "         <span>Образовательный центр:</span>\n" +
        "         <p>" + education_centre + "</p>\n" +
        "        </li>\n" +
        "       </ul>\n" +
        "      </div>\n" +
        "      <div class=\"col s12 m12 l7\">\n" +
	    "       <h3 class=\"center teal-text\">" + name + "</h3>" +
        "       <p class=\"p-item p-item-descr\">" + short_text + "</p>\n" +
        "      </div>\n" +
        "     </div>\n" +
        "     <div class=\" col s12 divider\"></div>\n" +
        "    </div>";
}

function e(query){
    return document.querySelector(query);
}

async function loadPosts(params){
    if(loadingPosts){
        return;
    }

    await loadNames(); // Для корректного отображения имен, если вдруг добавилась новая организация/город/etc

    loadingPosts = true;
    let externalData = "";

    const checkParam = (n) => {
        if(params[n] !== undefined){
            externalData += "&" + n + "=" + encodeURIComponent(params[n])
        }else{
            externalData += "&" + n + "="
        }
    };


    checkParam("classes");
    checkParam("lesson");
    checkParam("education_centre");
    checkParam("location");
    checkParam("date_start");
    checkParam("date_end");
    checkParam("sort");
    checkParam("offset");

    const posts = document.querySelector("#posts");
    posts.innerHTML = spinner;

    const xhr = new XMLHttpRequest();
    xhr.open("GET", "/api/?method=find"+externalData);
    console.log("url="+"/api/?method=find"+externalData);
    xhr.send(null);

    xhr.onreadystatechange = () => {
        if(xhr.readyState === XMLHttpRequest.DONE){
            let readyHtml = "";

            let response = JSON.parse(xhr.responseText);
            if(response){
                if(response.status){
                    for(const data of response.response.items){
                        const {id, name, classes, lesson, education_centre, location, date_start, date_end, short_text, link} = data;

                        readyHtml += makePost(name, parseClasses(classes), unix2date(parseInt(date_start)), unix2date(parseInt(date_end)), names.location[location], names.lesson[lesson], names.education_centre[education_centre], short_text, link);
                    }
                }else{
                    Toast.error("Can't make query due to "+response.error);
                }
            }

            setTimeout(() => {
                if(readyHtml.length === 0){
                    readyHtml =
                        "<br />" +
                        "<br />" +
                        "<h5>Олимпиад по вашему запросу не найдено</h5>" +
                        "<br />" +
                        "<br />";
                }
                posts.innerHTML = readyHtml;
                makePagination(response.response.offset, response.response.count_all);
                loadingPosts = false;
            }, 200);
        }
    };
}

function makePagination(offset, count_all){
	const posts_per_page = 5;
	const pages_count = Math.floor(count_all/posts_per_page)+1;
	const current_page = Math.floor(offset/5)+1;

	const canGoLeft = current_page > 1;
	const canGoRight = current_page < pages_count;

	const chevronLeftClass = canGoLeft ? "waves-effect" : "disabled";
	const chevronRightClass = canGoRight ? "waves-effect" : "disabled";

	const aLeftOnClick = canGoLeft ?
		(e) => { // Можно перейти влево
			tryLoadPosts()
		}:
		(e) => { // Нельзя перейти влево
			e.preventDefault();
			return false;
		};

	document.querySelector("#pagination").innerHTML =
		"<li class=\"" + chevronLeftClass + "\"><a href=\"#\"><i class=\"material-icons\">chevron_left</i></a></li>\n" +
		"<li class=\"active\"><a href=\"#\">" + current_page + "</a></li>\n" +
		"<li class=\"" + chevronRightClass + "\"><a href=\"#\"><i class=\"material-icons\">chevron_right</i></a></li>";
}

function tryLoadPosts(mobile = false, offset = 0){

    let classes, lesson, education_centre, location, date_start, date_end;

    const prefix = mobile ? "mob-" : "";

    const getVal = (val, multiple = false) => {
        return multiple ?
            getSelectValues(e("#"+prefix+val)).join(","):
            e("#"+prefix+val).value;
    };

    classes = getVal("classes", true);
    lesson = getVal("lesson", true);
    education_centre = getVal("education_centre", true);
    location = getVal("location", true);
    date_start = getVal("date_start");
    date_end = getVal("date_end");
    offset = 0;

    // noinspection JSIgnoredPromiseFromCall
	loadPosts({
        sort: "last",
        classes: classes,
        lesson: lesson,
        education_centre: education_centre,
        location: location,
        date_start: date_start,
        date_end: date_end,
        offset: offset,
    });
}

const Toast = {
    success(message){
        M.toast({html:message,classes:"green"});
    },
    error(message){
        M.toast({html:message,classes:"red"});
    }
};

const modals = M.Modal.init(document.querySelectorAll('.modal'), {
    preventScrolling: true,
    onCloseStart: () => {
        tryLoadPosts(true);
    }
});

const bottomSheetModal = modals[0];


function getSelectValues(select) {
    let result = [];
    let options = select && select.options;
    let opt;

    for (let i = 0, iLen=options.length; i<iLen; i++) {
        opt = options[i];

        if (opt.selected) {
            result.push(opt.value || opt.text);
        }
    }
    return result;
}

function unix2date(unix){

    let months_arr = ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'];

    let date = new Date(unix*1000);

    let year = date.getFullYear();
    let month = months_arr[date.getMonth()];
    let day = date.getDate();

    return day + ' ' + month + ' ' + year;
}

function parseClasses(classes){
    classes = classes.sort((a, b) => parseInt(a)-parseInt(b));

    return classes.join(",");
}


tryLoadPosts();
loadRecent();