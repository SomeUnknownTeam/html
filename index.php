<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Олимпиады</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/style.css?<?=time()?>">
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
</head>

<body>

<div id="search-settings" class="modal bottom-sheet">
	<a href="#" class="modal-close modal-right">
		<i class="material-icons modal-size teal-text">done</i>
	</a>
	<a href="#" class="modal-close modal-left">
		<i class="material-icons modal-size grey-text">close</i>
	</a>
	<div class="modal-content">
		<br />
		<br />
		<div class="input-field">
			<select multiple id="mob-classes">
				<option value="1">1 класс</option>
				<option value="2">2 класс</option>
				<option value="3">3 класс</option>
				<option value="4">4 класс</option>
				<option value="5">5 класс</option>
				<option value="6">6 класс</option>
				<option value="7">7 класс</option>
				<option value="8">8 класс</option>
				<option value="9">9 класс</option>
				<option value="10">10 класс</option>
				<option value="11">11 класс</option>
			</select>
			<label for="mob-classes">Возраст</label>
		</div>
		<br />
		<div class="input-field">
			<select multiple id="mob-lesson">
				<option value="math">Математика</option>
				<option value="it">Информатика</option>
			</select>
			<label for="mob-lesson">Дисциплина</label>
		</div>
		<br />
		<div class="input-field">
			<select multiple id="mob-education_centre">
				<option value="1">7 пядей</option>
				<option value="2">Кванториум</option>
				<option value="3">ДДЮТ</option>
			</select>
			<label for="mob-education_centre">Образовательный центр</label>
		</div>
		<br />
		<div class="input-field">
			<select multiple id="mob-location">

				<option value="izhevsk">Ижевск</option>
				<option value="kazan">Казань</option>
				<option value="3">Самара</option>
			</select>
			<label for="mob-location">Место проведения</label>
		</div>
		<br />
		<div class="input-field">
			<i class="material-icons prefix">date_range</i>
			<input type="text" class="datepicker" id="mob-date_start">
			<label for="mob-date_start"> Начиная </label>
		</div>
		<br />
		<div class="input-field">
			<i class="material-icons prefix">date_range</i>
			<input type="text" class="datepicker" id="mob-date_end">
			<label for="mob-date_end"> Заканчивая </label>
		</div>
	</div>
	<div class="modal-footer">
	</div>
</div>

<div class="navbar-fixed">
	<nav class="teal lighten-1">
		<div class="nav-wrapper">
			<a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
			<div class="container">
				<a href="#" class="brand-logo">RStudy</a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="#">First</a></li>
					<li><a href="#">Second</a></li>
					<li><a href="#">Third</a></li>
				</ul>
			</div>
		</div>
	</nav>
</div>
<ul id="slide-out" class="sidenav">
	<li><a class="waves-effect" href="#">First</a></li>
	<li><a class="waves-effect" href="#">Second</a></li>
	<li><a class="waves-effect" href="#">Third</a></li>
</ul>
<div>&nbsp;</div>
<div class="container">
	<div class="row">
		<div class="col s12 m12 l9 main-block">
			<div class="selectors hide-on-small-only">
				<div class="input-field col s12 m4">
					<select multiple id="classes">
						<option value="1">1 класс</option>
						<option value="2">2 класс</option>
						<option value="3">3 класс</option>
						<option value="4">4 класс</option>
						<option value="5">5 класс</option>
						<option value="6">6 класс</option>
						<option value="7">7 класс</option>
						<option value="8">8 класс</option>
						<option value="9">9 класс</option>
						<option value="10">10 класс</option>
						<option value="11">11 класс</option>
					</select>
					<label for="classes">Возраст</label>
				</div>
				<div class="input-field col s12 m4">
					<select multiple id="lesson">
						<option value="math">Математика</option>
						<option value="it">Информатика</option>
						<option value="3">other</option>
						<option value="4">other</option>
						<option value="5">other</option>
						<option value="6">other</option>
					</select>
					<label for="lesson">Дисциплина</label>
				</div>
				<div class="input-field col s12 m4">
					<select multiple id="education_centre">
						<option value="1">7 пядей</option>
						<option value="2">Кванториум</option>
						<option value="3">ДДЮТ</option>
					</select>
					<label for="education_centre">Образовательный центр</label>
				</div>
				<div class="input-field col s12 m4">
					<select multiple id="location">

						<option value="1">Ижевск</option>
						<option value="2">Казань</option>
						<option value="3">Самара</option>
					</select>
					<label for="location">Место проведения</label>
				</div>
				<div class="input-field col s12 m4">
					<i class="material-icons prefix">date_range</i>
					<input type="text" class="datepicker" id="date_start">
					<label for="date_start"> Начиная </label>
				</div>
				<div class="input-field col s12 m4">
					<i class="material-icons prefix">date_range</i>
					<input type="text" class="datepicker" id="date_end">
					<label for="date_end"> Заканчивая </label>
				</div>
			</div>
			<br />
			<br />

			<div class="items-group" id="posts">

			</div>

			<div class="row">
				<div class="col s12">
					<div class="center-align">
						<ul class="pagination" id="pagination">

						</ul>
					</div>
				</div>
			</div>

		</div>

		<div class="col s12 m12 l3 latestadd">
			<h4>Последнее добавленное</h4>
			<div class="collection" id="recent">
			</div>

			<br />
			<br />
		</div>
	</div>
</div>

<button onclick="bottomSheetModal.open();" class="open-settings-query-button btn btn-large teal waves-effect waves-lighten hide-on-med-and-up">
	Настройки поиска
</button>


<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script type="text/javascript" src="assets/js/script.js"></script>
<script src="main.js?<?=time()?>" defer></script>
</body>

</html>
