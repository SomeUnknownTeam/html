  $(document).ready(function(){
    $('.sidenav').sidenav();
  });
  
  $(document).ready(function() {
    $('select').formSelect();
  });

  $(document).ready(function() {
    $('.datepicker').datepicker({
      selectMonths: true,
      format: 'dd, mmm, yyyy',
      i18n: {
        cancel: 'Отменить',
        today: 'Сегодня',
        clear: 'Очистить',
        close: 'Закрыть',
        labelMonthNext: 'Next month',
        labelMonthPrev: 'Previous month',
        labelMonthSelect: 'Select a month',
        labelYearSelect: 'Select a year',
        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
          'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
        ],
        monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
          'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'
        ],
        weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        weekdaysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        weekdaysAbbrev: ['В', 'П', 'В', 'С', 'Ч', 'П', 'С'],
      },
      firstDay: 1,
    });
  });
